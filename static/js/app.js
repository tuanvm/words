/**
 * Load app
 **/
;(function($, db, moment){
    var storage;
    db.open({
        server:"words",
        version:2,
        schema:{
            post:{
                key:{
                    keyPath:"date"//,
                    //autoIncrement:true
                },
                indexes:{
                    date:{
                        unique:true
                    }
                }
            }
        }
    }).done(function(s){
        storage = s;

        populate();

        window.db = s;
    });

    var goal = 750;
    var save_interval = null;
    var save_timeout = null;
    var nav = document.querySelector("nav");
    var textarea = document.getElementById("editor");
    var saveButton = document.getElementById("save");
    var prev = document.querySelector("a.prev");
    var current = document.querySelector("a.current");
    var next = document.querySelector("a.next");

    function date(){
        var hash = window.location.hash.replace(/^#/, "");
        var requestedDate = moment(hash, "YYYY-MM-DD");
        if(requestedDate && requestedDate.isValid()){
            return requestedDate.toDate();
        } else {
            var d = new Date();
            return new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0);
        }
    }

    function value(el){
        return $(el).val() || "";
    }

    function words(el){
        var v = value(el);
        if(!v || v === ""){ return 0; }

        var matches = v.match(/(\w+)\b/g);
        if(!matches){ return 0; }
        
        return matches.length;
    }

    function get(key){
        return storage.query("post").filter("date", key).execute();
    }

    function save(el){
        if(!$(el).data("dirty")){ return; }
        
        var key = moment(date()).format("YYYY-MM-DD");
        var v = value(el);

        $(saveButton).text("SAVING...").prop("disabled", true);

        var markClean = function(){
            console.log("updated");
            $(el).data("dirty", false);
            $(saveButton).text("SAVED").prop("disabled", true);
        };

        var saveNew = function(){
            console.log("saving...");
            storage.add("post", {
                date:key,
                content:v
            }).done(markClean).fail(function(){
                
            });
        };

        get(key).done(function(records){
            if(!records || records.length === 0){
                return saveNew();
            }

            console.log("updating...");
            for(var i = 0, len = records.length; i < len; i++){
                records[i].content = v;
                storage.update("post", records[i]).done(markClean);
            }
        }).fail(saveNew);
    }

    function stats(el){
        var count = words(el);
        var to_go = goal - count;
        var d = date();
        if(to_go > 0){
            document.title = to_go + " left - " + moment(d).format("MMMM DD");
        } else {
            document.title = count + " words - " + moment(d).format("MMMM DD");
        }
    }

    function populate(){
        clearInterval(save_interval);
        clearTimeout(save_timeout);
        
        var d = date();
        var key = moment(d).format("YYYY-MM-DD");
        storage.query("post").filter("date", key).execute().done(function(records){
            var content = "";
            if(records.length > 0){
                content = records[0].content;
            }
            $(textarea).val(content).data("dirty", false);
            update_pagination(d);
            stats(textarea);
        });
    }

    function update_pagination(loaded_date){
        var d = moment(loaded_date);
        var now = moment();
        if(now.diff(d, "days") === 0){
            $(prev).attr("href", d.subtract("days", 1).format("[#]YYYY-MM-DD"));
            $(next).css("visibility", "hidden");
            $(current).text("Today");
        } else {
            $(prev).attr("href", d.clone().subtract("days", 1).format("[#]YYYY-MM-DD"));
            $(next).attr("href", d.clone().add("days", 1).format("[#]YYYY-MM-DD"));
            $(next).css("visibility", "visible");
            $(current).text(d.format("MMMM DD"));
        }

    }

    function begin_intervals(){
        console.log("starting intervals");
        save_interval = setInterval(function(){
            save(textarea);
        }, 30000);

        save_timeout = setTimeout(function(){
            save(textarea);
        }, 15000);
    }

    $(function(){
        window.addEventListener("hashchange", function(){
            var hash = window.location.hash.replace(/^#/,"");
            var d = moment(hash, "YYYY-MM-DD");
            if(d && d.isValid()){
                populate();
            }
        }, false);

        $(saveButton).click(function(){
            save(textarea);
        });

        $(textarea).keyup(function(e){
            $(this).data("dirty", true);
            $(saveButton).prop("disabled", false).text("SAVE");

            switch(e.which){
            case 8:
                setTimeout(function(){
                    stats(textarea);
                }, 1);
                break;
            case 13:
                setTimeout(function(){
                    //save(textarea);
                    stats(textarea);
                }, 1);
                break;
            case 32:
                setTimeout(function(){
                    stats(textarea);
                }, 1);
                break;
            case 46:
                setTimeout(function(){
                    stats(textarea);
                }, 1);
                break;
            }
        }).change(function(){
            console.log("change");
        });
        
        detectPaste(textarea, function(){
            stats(textarea);
        });

        textarea.focus();
    });

    function detectPaste(textarea, callback) {
        textarea.onpaste = function() {
            var sel = getTextAreaSelection(textarea);
            var initialLength = textarea.value.length;
            window.setTimeout(function() {
                var val = textarea.value;
                var pastedTextLength = val.length - (initialLength - sel.length);
                var end = sel.start + pastedTextLength;
                callback({
                    start: sel.start,
                    end: end,
                    length: pastedTextLength,
                    text: val.slice(sel.start, end)
                });
            }, 1);
        };
    }

    function getTextAreaSelection(textarea) {
        var start = textarea.selectionStart, end = textarea.selectionEnd;
        return {
            start: start,
            end: end,
            length: end - start,
            text: textarea.value.slice(start, end)
        };
    }

}(jQuery, window.db, window.moment));
